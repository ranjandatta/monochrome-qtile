# Monochrome Qtile Rice

## .config contains configs for:
* alacritty
* deadbeef
* dunst
* picom (ftlabs fork)
* qtile (with qtile extras)
* rofi

## .local conatins
* powermenu (rofi)
* autostart file
* wallpaper
* fonts

## .themes has the gtk theme (made with themix)

## firefox folder contains the userChrome and userContent css files

## for icons use [Tela Circle Grey](https://github.com/vinceliuice/Tela-circle-icon-theme)

## for vscodium use [Monochrome Amplified](https://github.com/anotherglitchinthematrix/monochrome) 


