import os
import subprocess
from libqtile import hook

from modules.ezkeys import *
from modules.groups import *
from modules.screens import *
from modules.layouts import *

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.local/bin/autostart.sh')
    subprocess.call([home])

# @hook.subscribe.setgroup
# def setgroup():
#     for i in qtile.groups:
#         if i is qtile.current_group:
#             i.set_label("󰁲")
#         else:
#             if i.windows:
#                 i.set_label("")
#             else:
#                 i.set_label("")
