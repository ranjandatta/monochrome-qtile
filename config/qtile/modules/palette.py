from dataclasses import dataclass

@dataclass(frozen=True)
class Mono:
    transparent=  '#00000000'
    background=   '#000000'
    foreground=   '#ffffff'
    white=        '#b5cabb'
    black=        '#202020'
    red=          '#6b6b6b'
    green=        '#c4c4c4'
    yellow=       '#b3b3b3'
    blue=         '#999999'
    magenta=      "#717171"
    cyan=         '#8a8a8a'
    gray=         '#464646'
    graylight=    '#7c7c7c'
    orange=       '#eeeeee'
    maroon=       '#8a8a8a'

class Gruvbox:
    transparent=  '#00000000'
    background=   '#282828'
    foreground=   '#d4be98'
    white=        '#f2e5bc'
    black=        '#3c3836'
    red=          '#ea6962'
    green=        '#a9b665'
    yellow=       '#d8a657'
    blue=         '#7daea3'
    magenta=      "#d3869b"
    cyan=         '#89b482'
    gray=         '#45403d'
    graylight=    '#654735'
    orange=       '#b47109'
    maroon=       '#c14a4a'

class Everforest:
    transparent=  '#00000000'
    background=   '#2d353b'
    foreground=   '#d3c6aa'
    white=        '#fffbef'
    black=        '#414b50'
    red=          '#e67e80'
    green=        '#a7c080'
    yellow=       '#dbbc7f'
    blue=         '#7fbbb3'
    magenta=      "#d699b6"
    cyan=         '#83c092'
    gray=         '#475258'
    graylight=    '#5c6a72'
    orange=       '#dfa000'
    maroon=       '#f85552'

palette = Mono()
