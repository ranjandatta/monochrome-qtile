from libqtile.config import Group, ScratchPad, DropDown, EzKey as Key
from libqtile.lazy import lazy
from modules.ezkeys import keys
from modules.prefs import terminal 

def group(group_labels):
    group = []
    group_names = ["1", "2", "3", "4"]
    for i in range(len(group_names)):
        group.append(Group(name=group_names[i], label=group_labels[i]))
    return group

groups = group(
    ["", "", "", ""]
)
 
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                'M-'+i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                'M-S-'+i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )


# Scratchpads
groups.append( 
    ScratchPad("pads",[
        DropDown("top", terminal + " -e btm", x=0.15, y=0.2, width=0.70, height=0.65, on_focus_lost_hide=False),
        DropDown("music", "deadbeef", x=0.15, y=0.2, width=0.70, height=0.65, on_focus_lost_hide=False),
    ]) 
)

keys.extend([
   Key("M-C-t", lazy.group['pads'].dropdown_toggle('top')),
   Key("M-C-m", lazy.group['pads'].dropdown_toggle('music')),
])

