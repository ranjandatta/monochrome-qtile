import subprocess
from functools import partial
from libqtile.widget import base
from libqtile.log_utils import logger

class DeadBeef(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('update_interval',0.5),
    ]

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self,"", **config)
        self.add_defaults(DeadBeef.defaults)
        self.add_callbacks({
            'Button1': partial(subprocess.Popen,['deadbeef','--play-pause']),
            'Button4': partial(subprocess.Popen,['deadbeef','--next']),
            'Button5': partial(subprocess.Popen,['deadbeef','--prev']),
        })

    def get_info(self):
        try:
            output = subprocess.run(['deadbeef --nowplaying-tf "Current:%album%:%artist%:%title%:%ispaused%"'], shell=True, capture_output=True,).stdout.decode()
        except subprocess.CalledProcessError as err:
            output = err.output.decode()

        if output.startswith("Current"):
            output = output.split(':')
            
            info = {'Album':        "",
                    'Artist':       "",
                    'Title':        "",
                    }
            
            info['Album'] =output[1]
            info['Artist']=output[2]
            info['Title'] =output[3]
            info['Paused']=output[4]
            return info

    def now_playing(self):
        """return a string with the now playing info (Artist - Song Title)."""
        info = self.get_info()
        now_playing = "󰐊 Not running"
        if info:
            title = info['Title']
            artist = info['Artist']

            if info['Paused']:
                now_playing = "󰐊 {0} - {1}".format(artist,title)
            else:
                now_playing = "󰏤 {0} - {1}".format(artist,title)
        return now_playing


    def poll(self):
        """Pool content for the text box"""
        return self.now_playing()