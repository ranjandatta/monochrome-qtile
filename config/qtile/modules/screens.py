import os
from libqtile.config import Screen, Group, DropDown
from libqtile import bar, qtile, widget
from libqtile.lazy import lazy

from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

from modules.palette import palette
from modules.prefs import browser, terminal


widget_defaults = dict(
    font="Proxima Nova Alt SemiBold",
    fontsize = 16,
    padding = 12,
    foreground = palette.foreground,
)

bar_defaults = {
    'size': 28, 
    'border_width': 4, 
    'background': palette.background,
    'border_color': palette.background,
    'margin': 0
}

extension_defaults = widget_defaults.copy()

def border(color: str, size=2):
    return { 
        "decorations":[
            BorderDecoration(
                colour = color,
                border_width = [0, 0, size, 0],
            )
        ]
    }
    
def init_widgets_list():
    widgets_list = [
        widget.Spacer(length=8),
        widget.CurrentLayoutIcon(
            padding = 7,
            scale = 0.45,
            **border(palette.magenta)
        ),
        widget.Spacer(length=8),
        widget.GroupBox(
            active =            palette.blue,
            inactive =          palette.gray,
            borderwidth =       0,
            highlight_method =  "text",
            this_current_screen_border=palette.cyan,
            visible_groups =["1", "2", "3", "4"],
            **border(palette.cyan)
        ),
        widget.Spacer(),
        widget.WindowName(
            width = bar.CALCULATED,
            **border(palette.foreground),
            empty_group_string = '',
        ),
        widget.Spacer(),
        widget.DF(
            format='  {r:.0f}%',
            visible_on_warn=False,
            foreground=palette.maroon,
            **border(palette.maroon),
        ),
        widget.Spacer(length=8),        
        widget.Memory(
            format='󰍛  {MemPercent}%',
            foreground=palette.blue,
            **border(palette.blue)
        ),
        widget.Spacer(length=8),        
        widget.CPU(
            format='  {load_percent}%',
            foreground=palette.magenta,
            **border(palette.magenta)
        ),
        widget.Spacer(length=8),
        widget.PulseVolume(
            volume_app = 'pavucontrol',
            foreground = palette.graylight,
            emoji=True,
            emoji_list=['󰝟', '󰕿', '󰖀', '󰕾'],
            fontsize=20,
            **border(palette.graylight)
        ),
        widget.PulseVolume(
            volume_app = 'pavucontrol',
            foreground = palette.graylight,
            padding = 0,
            **border(palette.graylight)
        ),
        widget.Spacer(length=12, **border(palette.green)),
        widget.Spacer(length=8),
        widget.Clock(
            format="󰔛  %I:%M",
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(browser + ' https://time.is')},
            foreground = palette.orange,
            **border(palette.orange)
        ),
        widget.Spacer(length=8),        
    ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1 

def init_screens():
	return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), **bar_defaults)),
    ]

screens = init_screens()
