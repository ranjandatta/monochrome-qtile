from libqtile import layout
from libqtile.lazy import lazy
from libqtile.config import EzClick as Click, EzDrag as Drag, Match

from modules.palette import palette

layout_theme = {
    "border_width": 2,
    "margin": 10,
    "border_focus": [palette.green],
    "border_normal": [palette.gray],
    "grow_amount": 5,
}

layouts = [
    layout.Columns(num_columns=3, insert_position=1, **layout_theme),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Floating(**layout_theme),
    layout.Zoomy(**layout_theme),
]

floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="Pavucontrol"),  
        Match(wm_class="Nitrogen"),
        Match(wm_class="Lxappearance"),
    ],
    **layout_theme
)

# Drag floating layouts.
mouse = [
    Drag("M-1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag("M-3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click("M-2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = [] 
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = False
auto_minimize = True

wmname = "Qtile"
