#!/bin/bash
(
    flock 200

    killall -q polybar
    while pgrep -u $UID -x polybar > /dev/null; do sleep 0.5; done

    polybar --reload main </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
    nitrogen --restore
    
) 200>/var/tmp/polybar-launch.lock
