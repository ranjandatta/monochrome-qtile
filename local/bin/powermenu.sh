#! /bin/sh

rofi_cmd() {
	rofi -dmenu \
		-theme ~/.config/rofi/powermenu.rasi
}

chosen=$(printf "󰗽\n\n󰐥\n󰒲" | rofi_cmd)

case "$chosen" in
	"󰐥") systemctl poweroff ;;
	"") systemctl reboot ;;
	"󰗽") loginctl terminate-user `whoami` ;;
	"󰒲") sleep 600 && systemctl suspend;;
	*) exit 1 ;;
esac
